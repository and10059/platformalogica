﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class varCorectata_profesor
    {
        public int Id { get; set; }
        public int variantaId { get; set; }
        public bool corectata { get; set; }
    }
}