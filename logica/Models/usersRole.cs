﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class usersRole
    {
        public string username { get; set; }
        public string userID { get; set; }
        public List<role> roluri { get; set; }
    }
    public class role
    {
        public string roleName { get; set; }
        public string roleId { get; set; }
    }
}