﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class s1_a_deschise
    {
        [Key]
        public int ID { get; set; }
        public string xml_subiect { get; set; }
       public string nume { get; set; }
        public bool indisponibil { get; set; }
        public DateTime createDate { get; set; }
    }
    public class s1_a_rezultateElev {
        public int ID { get; set; }
        public string idElev { get; set; }
        public int idVarianta { get; set; }
        public DateTime dataOra { get; set; }
        public string ultimulPunctaj { get; set; }
        public string xml_listaRaspunsuri { get; set; }
        public bool inactiv { get; set; }
       

    }
}