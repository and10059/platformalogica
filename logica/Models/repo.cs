﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class repo
    {
        private logicaDb db = new logicaDb();

        public s1Cerinta addS1Cerinta(s1Cerinta model)
        {
            db.s1Table.Add(model);
            db.SaveChanges();
            return model;
        }

        public s1GrilaViewModel getS1GrilaByID(int id)
        {
            s1GrilaViewModel model = new s1GrilaViewModel();
            model.var = db.s1VarianteTable.Find(id);
            var tempList = (from m in db.s1GrilaCompleta
                            where m.var == id
                            select m.cerinta).ToList();
            List<s1Cerinta> lista = new List<s1Cerinta>();
            foreach (var item in tempList)
            {
                var mod = (from m in db.s1Table
                           where m.ID == item
                           select m).FirstOrDefault();
                lista.Add(mod);
            }
            model.cerinte = lista;
            model.s1B = db.s1BcerinteTable.Find(model.var.s1Bid);
                  
            model.s1b1 = db.s1bSVG.Find(model.s1B.svgId);
            return model;
        }

        public s3C  addNewS3C(s3C model)
        {
            db.s3Ctable.Add(model);
            db.SaveChanges();
            return model;
        }

        public varianta addNewVarS1(List<int> cerinteList,int s1B)
        {
            varianta model = new varianta();
            model.s1Bid = s1B;
            db.s1VarianteTable.Add(model);
            db.SaveChanges();
            foreach(var item in cerinteList)
            {
                s1Grila model2 = new s1Grila();
                model2.var = model.ID;
                model2.cerinta = item;
                db.s1GrilaCompleta.Add(model2);
                db.SaveChanges();    
            }
            return model;
        }

        public s3Cerinta addNewS3(s3Cerinta model)
        {
            db.s3Table.Add(model);
            db.SaveChanges();
            return model;
        }

        public  s2Cerinta addS2Complete(s2Cerinta model)
        {
            db.s2CerinteTable.Add(model);
            db.SaveChanges();
            return model;
        }

        public void removeVar(int id)
        {
           
            db.s1VarianteTable.Remove(db.s1VarianteTable.Find(id));
            List<s1Grila> listaGrile = new List<s1Grila>();
            listaGrile = (from m in db.s1GrilaCompleta
                          where m.var == id
                          select m).ToList();
            db.SaveChanges();
            foreach (var item in listaGrile)
            {
                db.s1GrilaCompleta.Remove(item);
                db.SaveChanges();
            }
            var s1b = (from m in db.s1B_LinkCerinteTable
                       where m.var == id
                       select m).FirstOrDefault();
            db.s1B_LinkCerinteTable.Remove(s1b);
            db.SaveChanges();

        }

        public s1B addS1BCerinta(s1B model,s1B_1 svg)
        {
            
            db.s1bSVG.Add(svg);
            db.SaveChanges();
            model.svgId = svg.ID;
            db.s1BcerinteTable.Add(model);
            db.SaveChanges();
            return model;

        }

        public void removeS1Cerinta(int id)
        {

            db.s1Table.Remove(db.s1Table.Find(id));
            db.SaveChanges();
            var list = (from m in db.s1GrilaCompleta
                        where m.cerinta == id
                        select m).ToList();
            foreach (var item in list)
            {
                s1Grila model = new s1Grila();
                model = item;
                db.s1GrilaCompleta.Remove(model);
                db.SaveChanges();
            }


        }

        //out of use
        public void removeS1BCerinta(int id)
        {

            db.s1BcerinteTable.Remove(db.s1BcerinteTable.Find(id));
            db.SaveChanges();
            var list = (from m in db.s1B_LinkCerinteTable
                        where m.cerinta == id
                        select m).ToList();
            foreach (var item in list)
            {
                s1BWithVar model = new s1BWithVar();
                model = item;
                db.s1B_LinkCerinteTable.Remove(model);
                db.SaveChanges();
            }
        }

        public s1B_1 getS1B1ById(int id)
        {
          
            return db.s1bSVG.Find(id);
        }

        public s2_prop s2PropAdd(s2_prop model)
        {
            db.s2PropTable.Add(model);
            db.SaveChanges();
            return model;
        }
     
        public s2E s2DAdd(s2E model)
        {
            db.s2DTable.Add(model);
            db.SaveChanges();
            return model;
        }

        public void s2SubiectAdd(s2Cerinta model)
        {
            db.s2CerinteTable.Add(model);
        }

        public s3Silogism addNewSilog(s3Silogism model)
        {

            db.s3SilogisTable.Add(model);
            db.SaveChanges();
            return model;

        }

        public variantaFinala addFinalVar(string name,int s1,int s2,int s3)
        {
            variantaFinala model = new variantaFinala();
            model.finalVarName = name;
            model.s1 = s1;
            model.s2 = s2;
            model.s3 = s3;
            model.createTime = DateTime.Now;
            db.finalVarTable.Add(model);
            db.SaveChanges();
            varCorectata_profesor mod = new varCorectata_profesor();
            mod.corectata = false;
            mod.variantaId = model.id;

            db.varianteCorectate_Tabel.Add(mod);
            db.SaveChanges();
            return model;
        }
        public s2Cerinta getS2CerintaById(int ID)
        {
            s2Cerinta model = new s2Cerinta();
            model = (from m in db.s2CerinteTable
                     where m.ID == ID
                     select m).FirstOrDefault();
            
            return model;
        }
        public s2ViewModel getS2ViewModel(int ID)
        {
            s2ViewModel model = new s2ViewModel();
            model.cerintaModel = getS2CerintaById(ID);
            List<s2_prop> lista = new List<s2_prop>();
            lista.Add(db.s2PropTable.Find(model.cerintaModel.p1ID));
            lista.Add(db.s2PropTable.Find(model.cerintaModel.p2ID));
            lista.Add(db.s2PropTable.Find(model.cerintaModel.p3ID));
            lista.Add(db.s2PropTable.Find(model.cerintaModel.p4ID));
            model.s2PropList = lista;
            model.s2E = db.s2DTable.Find(model.cerintaModel.EID);
            return model;
        }
        public s3ViewModel gets3ViewModel(int id)
        {
            s3ViewModel model = new s3ViewModel();
            model.cerintaModel = db.s3Table.Find(id);
            model.s3c = db.s3Ctable.Find(model.cerintaModel.C_ID);
            model.s3silog1 = db.s3SilogisTable.Find(model.cerintaModel.A_silogismID_1);
            model.s3silog2 = db.s3SilogisTable.Find(model.cerintaModel.A_silogismID_2);
            return model;
        }
        public finalVarViewModel getFullTest(int id)
        {
            finalVarViewModel model = new finalVarViewModel();
            var variantaID = (from m in db.finalVarTable
                              where m.id == id
                              select m).FirstOrDefault();
            model.numeleVariantei = variantaID.finalVarName;
            model.s1model = getS1GrilaByID(variantaID.s1);
            model.s2model = getS2ViewModel(variantaID.s2);
            model.s3model = gets3ViewModel(variantaID.s3);


            return model; 
        }

        public List<int> expected_s1A (int varID)
        {
            var varianta = getFullTest(varID);
            List<int> rezultate = new List<int>();

            foreach (var item in varianta.s1model.cerinte)
            {

                try
                {
                    rezultate.Add(Convert.ToInt32(item.answer));
                }
                catch
                {
                    rezultate.Add(-1);
                }

            }
            return rezultate;

            
        }

        public void adaugaRezultatTestare(rezultateVariantaElev model)
        {
            db.rezultateVar_table.Add(model);
            db.SaveChanges();

        }
        public rezultateVariantaElev get_rezultateVariantaElev_byName(string nume)
        {
            var model = (from m in db.rezultateVar_table
                         where m.numeElev == nume
                         select m).FirstOrDefault();
            return model;
        }
        public List<rezultateVariantaElev> getList_rezultateVariantaElev_byName(string nume)
        {
            var model = (from m in db.rezultateVar_table
                         where m.numeElev == nume
                         select m).ToList();
            return model;
        }

        public void adauga_s1VarDeschise(s1_a_deschise model)
        {
            db.varianteDeschise_s1_table.Add(model);
            db.SaveChanges();
        }

        public s1Cerinta obtine_s1Cerinta_dupaId(int id)
        {
            return db.s1Table.Find(id);

        }
       
    }
    public class logicaDb : DbContext
    {
        public DbSet<s1Cerinta> s1Table { get; set; }
        public DbSet<varianta> s1VarianteTable { get; set; }
      public DbSet<s1Grila> s1GrilaCompleta { get; set; }

        public DbSet<s1B> s1BcerinteTable { get; set; }
        public DbSet<s1BWithVar> s1B_LinkCerinteTable { get; set; }
        public DbSet<s1B_1> s1bSVG { get; set; }

        //s2Tables
        public DbSet<s2Cerinta> s2CerinteTable { get; set; }
        public DbSet<s2_prop> s2PropTable { get; set; }       
        public DbSet<s2E> s2DTable { get; set; }

        public DbSet<s3Cerinta> s3Table { get; set; }
        public DbSet<s3Silogism> s3SilogisTable { get; set; }
        public DbSet<s3C> s3Ctable { get; set; }
        public DbSet<variantaFinala> finalVarTable { get; set; }
        public DbSet<variantaXmlTable> variantsTable { get; set; }
        public DbSet<rezultateVariantaElev> rezultateVar_table { get; set; }
        public DbSet<s1_a_deschise> varianteDeschise_s1_table { get; set; }
        public DbSet<s1_a_rezultateElev> varianteDeschise_s1_rezultate { get; set; }
        public DbSet<variantaDisponibila> variantaDisponibila_elev { get; set; }
        public DbSet<varCorectata_profesor> varianteCorectate_Tabel { get; set; }
        public DbSet<userData> userDates { get; set; }
    }
}