﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class rezultateVariantaElev
    {
        public int id { get; set; }
        public int varianta_ID { get; set; }
        public string numeElev { get; set; }
        public int? punctajFinal { get; set; }
        public int s1_A { get; set; }
        public int s1_B { get; set; }
        public int s2_A { get; set; }
        public int s2_B { get; set; }
        public int s2_C { get; set; }
        public int s2_D { get; set; }
        public int s2_E { get; set; }
        public int s3_A { get; set; }
        public int s3_B { get; set; }
        public int s3_C { get; set; }
        public int s3_D { get; set; }
        public DateTime data { get; set; }
        public string xml_paginaCorectare { get; set; }    //variantaCorectata convertita in xml
    }
}