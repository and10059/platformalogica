﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class userData
    {
        public int ID { get; set; }
        public string userID { get; set; }
        public string nume { get; set; }
        public string prenume { get; set; }
        public string scoala { get; set; }
        public string telefon { get; set; }
        public DateTime dataInregistrare { get; set; }
    }
}