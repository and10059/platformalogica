﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class variantaDisponibila
    {
        public int ID { get; set; }
        public int variantaID { get; set; }
        public string elevId { get; set; }
        public bool activ { get; set; }
    }
}