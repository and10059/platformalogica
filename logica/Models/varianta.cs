﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{

    public class variantaFinala
    {
        public int id { get; set; }
        public string finalVarName { get; set; }
        public int s1 { get; set; }
        public int s2 { get; set; }
        public int s3 { get; set; }
        public DateTime createTime { get; set; }
       
    }
    public class varianta
    {
        public int ID { get; set; }
        public int s1Bid { get; set; }     
    }

    //lista cerinte s1
    public class s1Cerinta
    {
        public int ID { get; set; }
        public string nume { get; set; }
        public string cerinta { get; set; }
        public string r1 { get; set; }
        public string r2 { get; set; }
        public string r3 { get; set; }
        public string r4 { get; set; }
        public string answer { get; set; }

    }
    public class s1Grila
    {
        public int ID { get; set; }
        public int var { get; set; }
        public int cerinta { get; set; }
    }
    //ViewModel
    public class s1GrilaViewModel
    {
        public varianta var { get; set; }
        public List<s1Cerinta> cerinte { get; set; }
        public s1B s1B { get; set; }
        public s1B_1 s1b1 { get; set; }
    }
    public class s1B
    {
       public int ID
        {
            get;set;
        }
        public string cerinta { get; set; }
      //  public string a { get; set; }
       // public string b { get; set; }
        public string r1 { get; set; }
        public string r2 { get; set; }
        public string r3 { get; set; }
        public string r4 { get; set; }
        public string r5 { get; set; }
        public string r6 { get; set; }
        public bool answer_r1 { get; set; }
        public bool answer_r2 { get; set; }
        public bool answer_r3 { get; set; }
        public bool answer_r4 { get; set; }
        public bool answer_r5 { get; set; }
        public bool answer_r6 { get; set; }
        public int svgId { get; set; }

    }
    public class s1B_1
    {
      public int ID { get; set; }
      public int corect { get; set; }
      public string svgObj1 { get; set; }
      public string svgObj2 { get; set; }
      public string svgObj3 { get; set; }
      public string svgObj4 { get; set; }
    }    
    //no more needed
    public class s1BWithVar
    {
        public int ID { get; set; }
        public int var { get; set; }
        public int cerinta { get; set; }
    }
    public class s2Cerinta
    {
        public int ID { get; set; }
        public int p1ID { get; set; }
        public int p2ID { get; set; }
        public int p3ID { get; set; }
        public int p4ID { get; set; }
        public int A_1 { get; set; }
        public int A_2 { get; set; }
        public string b_type1 { get; set; }
        public string b_type2 { get; set; }
        public string b_type3 { get; set; }
        public string b_type4 { get; set; }
        public int b_prop1 { get; set; }
        public int b_prop2 { get; set; }
        public int b_prop3 { get; set; }
        public int b_prop4 { get; set; }
        public int C_1{ get; set; }
        public int C_2 { get; set; }
        public int D_prop { get; set; }
        public string D_tip1 { get; set; }
        public string D_tip2 { get; set; }
        public int EID { get; set; }

    }
    public class s2_prop
    { public int ID { get; set; }
      public string prop { get; set; }
      public string type { get; set; }


    }
    public class s2E
    {
        public int ID { get; set; }
        public string xRat { get; set; }
        public string yRat { get; set; }
        public string answer { get; set; }
        public string tipCerut { get; set; }



    }
    public class s3Cerinta
    {
        public int ID { get; set; }
        public int A_silogismID_1 { get; set; }
        public int A_silogismID_2 { get; set; }
        public string B_silog { get; set; }
        public int C_ID { get; set; }       
        public string D_def { get; set; } 
        public string D_term { get; set; }

    }
    public class s3C
    {

        public int ID { get; set; }
        public string silog { get; set; }
        /// <summary>
        /// termeniul mediu reprezinta tipul termenului cerut in silogism
        /// </summary>
        public string tMediu { get; set; }
        /// <summary>
        /// pMin reprezinta tipul propozitiei ceruta in silogism
        /// </summary>
        public string pMin { get; set; }
    
    }
    public class s3Silogism
    {
        public int ID { get; set; }
        public string maj { get; set; }
        public string min { get; set; }
       
        public string con { get; set; }
        public int  fig { get; set; }

    }
    public class s3ViewModel
    {
        public s3Cerinta cerintaModel { get; set; }
        public s3C s3c { get; set; }
        public s3Silogism s3silog1 { get; set; }
        public s3Silogism s3silog2 { get; set; }
    }
    public class s2ViewModel
    {
        public s2Cerinta cerintaModel { get; set; }
        public List<s2_prop> s2PropList { get; set; }
        public s2E s2E { get; set; }

    }
    public class finalVarViewModel
    {
        public string numeleVariantei { get; set; }
       public s1GrilaViewModel s1model { get; set; }
       public  s2ViewModel s2model { get; set; }
       public s3ViewModel s3model { get; set; }

    }
   



}