﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class variantaRezolvata
    {
        public int ID { get; set; }
        public string userId { get; set; }
        public string variantaId { get; set; }
        public int s1_1 { get; set; }
        public int s1_2 { get; set; }
        public int s1_3 { get; set; }
        public int s1_4 { get; set; }
        public int s1_5 { get; set; }
        public int s1_6 { get; set; }
        public int s1_7 { get; set; }
        public int s1_8 { get; set; }
        public int s1_9 { get; set; }
        public int s1_10 { get; set; }
        public int s1_bA { get; set; }

        public bool s1_b1 { get; set; }
        public bool s1_b2 { get; set; }
        public bool s1_b3 { get; set; }
        public bool s1_b4 { get; set; }
        public bool s1_b5 { get; set; }
        public bool s1_b6 { get; set; }

        public string s2_a1 { get; set; }
        public string s2_a2 { get; set; }
        public string s2_b1F { get; set; }
        public string s2_b2F { get; set; }
        public string s2_b3F { get; set; }
        public string s2_b4F { get; set; }
        public string s2_b1N { get; set; }
        public string s2_b2N { get; set; }
        public string s2_b3N { get; set; }
        public string s2_b4N { get; set; }

        public string s2_c_pr1F { get; set; }
        public string s2_c_pr2F { get; set; }
        public string s2_c_pr3F { get; set; }
        public string s2_c_pr4F { get; set; }
        public string s2_c_pr1F_con { get; set; }
        public string s2_c_pr2F_obv { get; set; }
        public string s2_c_pr3F_con { get; set; }
        public string s2_c_pr4F_obv { get; set; }
        public string s2_c_pr1N_conv { get; set; }
        public string s2_c_pr2N_obv { get; set; }
        public string s2_c_pr3N_conv { get; set; }
        public string s2_c_pr4N_obv { get; set; }
        public string s2_d_prop { get; set; }
        public string s2_d_conv { get; set; }
        public string s2_d_obvConv { get; set; }
        public string s2_d_convNat { get; set; }
        public string s2_d_obvConvNat { get; set; }
        public string s2_e_a_x { get; set; }
        public string s2_e_a_y{ get; set; }
        public string s2_e_a_x_convobv { get; set; }
        public string s2_e_a_x_rez { get; set; }
        public string s2_e_a_y_convobv { get; set; }
        public string s2_e_a_y_rez { get; set; }

        public string s2_e_b_x { get; set; }
        public string s2_e_b_y { get; set; }
        //public string s2_e_b_x_corect { get; set; }
        //public string s2_e_b_x_convobv { get; set; }
        //public string s2_e_b_x_form1 { get; set; }
        //public string s2_e_b_x_form2 { get; set; }
        //public string s2_e_b_x_respectand { get; set; }
       public string s2_e_b_y_corect { get; set; }
        public string s2_e_b_y_convobv { get; set; }
        public string s2_e_b_y_form1 { get; set; }
        public string s2_e_b_y_form2 { get; set; }
        public string s2_e_b_y_respectand { get; set; }

        public string s3_a_1_sil1F_maj { get; set; }
        public string s3_a_1_sil1F_min { get; set; }
        public string s3_a_1_sil1F_con { get; set; }
        public string s3_a_1_sil2F_maj { get; set; }
        public string s3_a_1_sil2F_min { get; set; }
        public string s3_a_1_sil2F_con { get; set; }
        public string s3_a_1_sil1N_maj { get; set; }
        public string s3_a_1_sil1N_min { get; set; }
        public string s3_a_1_sil1N_con { get; set; }
        public string s3_a_1_sil2N_maj { get; set; }
        public string s3_a_1_sil2N_min { get; set; }
        public string s3_a_1_sil2N_con { get; set; }

       // public int s3_a_2_venn_sil1_maj { get; set; }
       // public int s3_a_2_venn_sil1_min { get; set; }
       public string s3_a_2_venn_sil1 { get; set; }
        public bool s3_a_2_venn_sil1_valid { get; set; }
      //  public int s3_a_2_venn_sil2_maj { get; set; }
       // public int s3_a_2_venn_sil2_min { get; set; }
       public string s3_a_2_venn_sil2 { get; set; }
        public bool s3_a_2_venn_sil2_valid { get; set; }

        public string s3_b_maj_F { get; set; }
        public string s3_b_min_F { get; set; }
        public string s3_b_con_F { get; set; }
        public string s3_b_maj_N { get; set; }
        public string s3_b_min_N { get; set; }
       // public int s3_b_fig { get; set; }

        public string s3_c_term { get; set; }
        public string s3_c_premisa { get; set; }
        public string s3_d_reg { get; set; }
        public string s3_d_defGresita { get; set; }
        public string s3_d_reg2 { get; set; }
    }
    public class variantaXmlTable
    {
        public int ID { get; set; }
        public string UserId { get; set; }
        public int variantaID { get; set; }
        public string xml { get; set; }
        public bool corectata { get; set; } 

    }
}