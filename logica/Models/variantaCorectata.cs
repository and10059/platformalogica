﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace logica.Models
{
    public class variantaCorectata
    {
        public List<bool> s2_a { get; set; }
        public List<bool> s2_b { get; set; }
        public List<bool> s2_c { get; set; }
        public List<bool> s2_d { get; set; }
        public List<bool> s2_e { get; set; }
        public List<bool> s3_a { get; set; }
        public List<List<int>> s3_a_venn { get; set; }
        public List<bool> s3_b { get; set; }
        public List<bool> s3_c { get; set; }
        public List<bool> s3_d { get; set; }

    }
}