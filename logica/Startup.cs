﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(logica.Startup))]
namespace logica
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
