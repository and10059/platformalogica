﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using logica.Models;

namespace logica.Controllers
{
    [Authorize]
    public class s2Controller : Controller
    {

        private repo repo = new repo();
        private logicaDb db = new logicaDb();
        public ActionResult listaPropozitiiCategorice()
        {

            return View(db.s2PropTable.ToList()); 

        }
        public ActionResult listaPropModal()
        {

            return PartialView("listaPropModal",db.s2PropTable.ToList());

        }

        public ActionResult listaS2EModal()
        {

            return PartialView("listaS2EModal", db.s2DTable.ToList());
        }
        public JsonResult addNewProp(s2_prop model)
        {

            try
            {
                
                return Json(new { ok = true,model= repo.s2PropAdd(model)});
            }
            catch
            {
                return Json(new { ok = false });
            }
        }


        [HttpGet]
        public ActionResult createS2()
        {

            return PartialView("createS2");
        }
        [HttpPost]
        public JsonResult addNewS2E(s2E model)
        {

            try
            {
                
                return Json(new { ok = true, model = repo.s2DAdd(model) });
            }
            catch
            {
                return Json(new { ok = false });
            }
        }

        public JsonResult addS2Complete(s2Cerinta model)
        {
            try
            {
                
                return Json(new { ok = true, s2Model= repo.addS2Complete(model)});
            }
            catch
            {
                return Json(new { ok = false });
            }

           
        }

    }
}