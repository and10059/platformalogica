﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using logica.Models;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Microsoft.AspNet.Identity;
namespace logica.Controllers
{
    [Authorize]
    public class s1_aController : Controller
    {
        // GET: s1_a
        logicaDb db = new logicaDb();
        repo repository = new repo();
        // [Route("blitz/new")]
        public ActionResult Index()
        {
            return View(db.s1Table.ToList());
        }
        [HttpPost]
        public JsonResult adaugaCerinta(List<int> idList, string nume)
        {
            try
            {
                s1_a_deschise model = new s1_a_deschise();
                model.xml_subiect = obtain_xml(idList);
                model.nume = nume;
                model.createDate = DateTime.Now;
                repository.adauga_s1VarDeschise(model);
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false + " ex:" + ex.ToString());
            }
        }

        public object getXml(string xml)
        {

            //   var xml = db.variantsTable.Find(id).xml;
            StringReader strReader = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            object obj = null;
            try
            {
                strReader = new StringReader(xml);
                serializer = new XmlSerializer(typeof(List<int>));
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }

            return obj;
        }

        private string obtain_xml(object model)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(model.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, model);
            }
            catch (Exception ex)
            {
                throw new Exception("NU s-a putut serializa xml " + model.ToString());
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }


            }
            return sw.ToString();
        }
        [HttpGet]
        public ActionResult rezolvaSesiuneBlitz(int sesId)
        {
            var model = db.varianteDeschise_s1_table.Find(sesId);
            if (model.indisponibil == true && User.IsInRole("elev") == true)
            {
                return RedirectToAction("Index");
            }
            var caras = getXml(model.xml_subiect) as List<int>;
            List<s1Cerinta> listaCerinte = new List<s1Cerinta>();
            foreach (var item in caras)
            {
                s1Cerinta mod = new s1Cerinta();
                mod = repository.obtine_s1Cerinta_dupaId(item);
                listaCerinte.Add(mod);
            }
            ViewBag.sesID = sesId;
            return View(listaCerinte);
        }
        [HttpPost]
        public JsonResult rezolvaSesiuneBlitz(string punctaj, int varId, List<int> listaRaspunsuri)
        {
            try
            {
                s1_a_rezultateElev model = new s1_a_rezultateElev();
                model.idElev = User.Identity.GetUserName();
                model.idVarianta = varId;
                model.ultimulPunctaj = punctaj;
                model.dataOra = DateTime.Now;
                model.xml_listaRaspunsuri = obtain_xml(listaRaspunsuri);
                db.varianteDeschise_s1_rezultate.Add(model);
                db.SaveChanges();
                return Json(new { ok = true });
            }
            catch
            {
                return Json(new { ok = false });
            }
        }


        [HttpGet]
        public ActionResult varianteBlitzDisponibile()
        {
            if (User.IsInRole("elev"))
            {
                var model = (from m in db.varianteDeschise_s1_table
                             where m.indisponibil == false
                             select m).ToList();
                return View(model);
            }
            else
            {
                return View(db.varianteDeschise_s1_table.ToList());
            }
        }
        [HttpGet]
        public JsonResult seteazaStareVarianta(int id)
        {
            try
            {
                var model = db.varianteDeschise_s1_table.Find(id);
                if (model.indisponibil == true)
                {
                    model.indisponibil = false;

                }
                else
                {
                    model.indisponibil = true;
                }
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { ok = true, status = model.indisponibil }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult varianteBlitzRezolvate()
        {
            string nume = User.Identity.GetUserName();
            var model = (from m in db.varianteDeschise_s1_rezultate
                         where m.idElev == nume && m.inactiv == false
                         select m).ToList();
            return PartialView("varianteBlitzRezolvate", model);
        }
        public JsonResult seteazaStareRezultat(int ID)
        {
            try
            {
                s1_a_rezultateElev model = db.varianteDeschise_s1_rezultate.Find(ID);

                model.inactiv = true;
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        public ActionResult vizualizareSesiuneBlitz(int sesId, int id)
        {

            var model = db.varianteDeschise_s1_table.Find(sesId);
            if (model.indisponibil == true && User.IsInRole("elev") == true)
            {
                return RedirectToAction("varianteBlitzDisponibile");
            }
            var caras = getXml(model.xml_subiect) as List<int>;
            List<s1Cerinta> listaCerinte = new List<s1Cerinta>();
            foreach (var item in caras)
            {
                s1Cerinta mod = new s1Cerinta();
                mod = repository.obtine_s1Cerinta_dupaId(item);
                listaCerinte.Add(mod);
            }
            var rezultate = (from m in db.varianteDeschise_s1_rezultate
                             where m.ID == id
                             select m).FirstOrDefault();
            ViewBag.rezultate = rezultate;
            ViewBag.listaRaspunsuriXML = getXml(rezultate.xml_listaRaspunsuri);
            ViewBag.dataOra = rezultate.dataOra;
            return View(listaCerinte);
        }
        public ActionResult denumireVarianta_partial(int id)
        {
            try
            {
                var nume = (from m in db.varianteDeschise_s1_table
                            where m.ID == id
                            select m).FirstOrDefault();
                ViewBag.nume = nume.nume;
                return PartialView("denumireVarianta_partial");

            }
            catch
            {
                return PartialView("denumireVarianta_partial", id);
            }
        }

    }
}