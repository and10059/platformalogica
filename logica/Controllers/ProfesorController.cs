﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using logica.Models;
using System.IO;
using System.Xml.Serialization;
using IronPdf;

namespace logica.Controllers
{
    [Authorize(Roles = "admin,profesor")]
    public class ProfesorController : Controller
    {
        private logicaDb db = new logicaDb();
        private repo repository = new repo();
        private ApplicationDbContext context = new ApplicationDbContext();
        // GET: Profesor
        
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult VizualizareRezultateBlitz()
        {
            return View(db.varianteDeschise_s1_rezultate.ToList());
        }
        public object getXml(string xml)
        {

            //   var xml = db.variantsTable.Find(id).xml;
            StringReader strReader = null;
            XmlSerializer serializer = null;
            System.Xml.XmlTextReader xmlReader = null;
            object obj = null;
            try
            {
                strReader = new StringReader(xml);
                serializer = new XmlSerializer(typeof(List<int>));
                xmlReader = new System.Xml.XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }

            return obj;
        }


        public ActionResult vizualizareSesiuneBlitz(int sesId, int id)
        {

            var model = db.varianteDeschise_s1_table.Find(sesId);
            if (model.indisponibil == true && User.IsInRole("elev") == true)
            {
                return RedirectToAction("Index");
            }
            var caras = getXml(model.xml_subiect) as List<int>;
            List<s1Cerinta> listaCerinte = new List<s1Cerinta>();
            foreach (var item in caras)
            {
                s1Cerinta mod = new s1Cerinta();
                mod = repository.obtine_s1Cerinta_dupaId(item);
                listaCerinte.Add(mod);
            }
            var rezultate = (from m in db.varianteDeschise_s1_rezultate
                             where m.ID == id
                             select m).FirstOrDefault();
            ViewBag.rezultate = rezultate;
            ViewBag.listaRaspunsuriXML = getXml(rezultate.xml_listaRaspunsuri);
            ViewBag.dataOra = rezultate.dataOra;
            return View(listaCerinte);
        }


        public ActionResult asignareVariantaElev(int varId)
        {
            ViewBag.varId = varId;
            var list = (from m in context.Users
                        select m.Email).ToList();
            List<bool> listBool = new List<bool>();
            foreach (var item in list)
            {
                listBool.Add(verificaDisponibilitateVariantaElev(varId, item));
            }
            ViewBag.activ = listBool;

            return View(list);
        }
        public JsonResult modificaPermisiune(string email, int varId)
        {
            try
            {
                var model = (from m in db.variantaDisponibila_elev
                             where m.elevId == email && m.variantaID == varId
                             select m).FirstOrDefault();
                if (model == null)
                {
                    variantaDisponibila mod = new variantaDisponibila();
                    mod.elevId = email;
                    mod.variantaID = varId;
                    mod.activ = true;
                    db.variantaDisponibila_elev.Add(mod);
                    db.SaveChangesAsync();
                    return Json(new { activ = true });
                }
                else
                {
                    model.activ = !model.activ;
                    db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { activ = model.activ });
                }

            }
            catch
            {
                return Json(false);
            }
        }

        public ActionResult listaVariante()
        {
            return View(db.finalVarTable.ToList());
        }
        public ActionResult partialVariantaCorectata(int varId)
        {
            var corectat = (from m in db.varianteCorectate_Tabel
                            where m.variantaId == varId
                            select m.corectata).FirstOrDefault();
            var list = (from m in db.variantsTable
                        where m.variantaID == varId
                        && m.corectata == false
                        select m
                ).ToList();
            int listaInt = 0;
            foreach (var item in list)
            {
                listaInt++;

            }
            ViewBag.varianteRamase = listaInt;
            ViewBag.id = varId;
            return PartialView("partialVariantaCorectata", corectat);
        }

        public JsonResult marcheazaCorectat(int varId)
        {
            try
            {
                var model = (from m in db.varianteCorectate_Tabel
                             where m.variantaId == varId
                             select m).FirstOrDefault();
                model.corectata = true;
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        public ActionResult vizualizareRezultateElevi()
        {

            return View(db.rezultateVar_table.ToList());

        }

        public ActionResult varianteBlitzDisponibile()
        {
            return View(db.varianteDeschise_s1_table.ToList());
        }

        public bool verificaDisponibilitateVariantaElev(int varId, string userName)
        {
            var model = (from m in db.variantaDisponibila_elev
                         where m.elevId == userName && m.variantaID == varId
                         select m).FirstOrDefault();
            if (model == null || model.activ == false)
            {
                return false;

            }
            else { return true; }
        }

        public ActionResult pdfVar(int varId)
        {
            return PartialView("pdfVar", repository.getFullTest(varId));
        }



        public ActionResult pdf(int id)
        {
            HtmlToPdf HtmlToPdf = new IronPdf.HtmlToPdf();
            var url = @"" + Url.Action("pdfVar", "Profesor", null, Request.Url.Scheme).ToString();
            url += "?varId=" + id;
            var PDF = HtmlToPdf.RenderUrlAsPdf(url);
            PDF.SaveAs(Server.MapPath("/App_Data/file.pdf"));

            string filename = Server.MapPath("/App_Data/file.pdf");
            return File(filename, "application/pdf", "Varianta " + id);
        }
        public ActionResult pdfBlitz(int id)
        {
            HtmlToPdf HtmlToPdf = new IronPdf.HtmlToPdf();
            var url = @"" + Url.Action("blitzPdf", "Profesor", null, Request.Url.Scheme).ToString();
            url += "?sesId=" + id;
            var PDF = HtmlToPdf.RenderUrlAsPdf(url);
            PDF.SaveAs(Server.MapPath("/App_Data/file.pdf"));

            string filename = Server.MapPath("/App_Data/file.pdf");
            return File(filename, "application/pdf", "Varianta " + id);
        }


        public ActionResult blitzPdf(int sesId)
        {
            var model = db.varianteDeschise_s1_table.Find(sesId);
            if (model.indisponibil == true && User.IsInRole("elev") == true)
            {
                return RedirectToAction("Index");
            }
            var caras = getXml(model.xml_subiect) as List<int>;
            List<s1Cerinta> listaCerinte = new List<s1Cerinta>();
            foreach (var item in caras)
            {
                s1Cerinta mod = new s1Cerinta();
                mod = repository.obtine_s1Cerinta_dupaId(item);
                listaCerinte.Add(mod);
            }
            ViewBag.sesID = sesId;
            return PartialView("blitzPdf",listaCerinte);
        }


    }
}