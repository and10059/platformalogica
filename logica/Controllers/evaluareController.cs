﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using logica.Models;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Microsoft.AspNet.Identity;

namespace logica.Controllers
{
    [Authorize]
    public class evaluareController : Controller
    {
        private logicaDb db = new logicaDb();
        private repo repository = new repo();

        [HttpGet]
        public ActionResult testare(int id)
        {

            return View(repository.getS1GrilaByID(id));
        }
        [HttpGet]
        public ActionResult testareS1B(int id)
        {
            //tesatre 1b

            return View();
        }

        [HttpGet]
        public ActionResult variantaNoua()
        {

            return View();
        }
        [HttpPost]
        public JsonResult variantaNoua(string name, int s1id, int s2id, int s3id)
        {

            
            return Json(new { ok = true, model = repository.addFinalVar(name, s1id, s2id, s3id) });
        }

        [HttpGet]
        [Authorize]
        public ActionResult testareFull(int id)
        {
            ViewBag.userName = User.Identity.GetUserName().ToString();
            ViewBag.variantaID = id;
            return View(repository.getFullTest(id));

        }
        [HttpGet]
        public ActionResult listaVarianteFinale()
        {
            var elevId = User.Identity.GetUserName();
            var varianteDisponibile = (from m in db.variantaDisponibila_elev
                                       where m.elevId == elevId && m.activ==true
                                       select m).ToList();
            List<variantaFinala> listaVariante = new List<variantaFinala>();
            foreach (var item in varianteDisponibile)
            {
                variantaFinala model = (from m in db.finalVarTable
                                        where m.id == item.variantaID
                                        select m).FirstOrDefault();
                listaVariante.Add(model);
            }
            return View(listaVariante);
        }
        public ActionResult venn()
        {
            return View();
        }
        [HttpGet]
        public ActionResult VennPartial(int id, int order)
        {
            ViewBag.diagNr = id;
            ViewBag.order = order;
            return PartialView("VennPartial");

        }
        public ActionResult vennPartialConcluzie(s3Silogism sil)
        {

            return PartialView("vennPartialConcluzie", sil);
        }

        [ValidateInput(false)]
        public JsonResult saveSolvedVar(variantaRezolvata model, int variantaID)
        {
            try
            {
                StringWriter sw = new StringWriter();
                XmlTextWriter tw = null;
                try
                {
                    XmlSerializer serializer = new XmlSerializer(model.GetType());
                    tw = new XmlTextWriter(sw);
                    serializer.Serialize(tw, model);
                }
                catch (Exception ex)
                {
                    //Handle Exception Code
                }
                finally
                {
                    sw.Close();
                    if (tw != null)
                    {
                        tw.Close();
                    }
                    

                    variantaXmlTable xmlModel = new variantaXmlTable();
                    xmlModel.UserId = User.Identity.GetUserName();
                    xmlModel.xml = sw.ToString();
                    xmlModel.variantaID = variantaID;
                    xmlModel.corectata = false;
                    db.variantsTable.Add(xmlModel);
                    db.SaveChanges();
                    var userName = User.Identity.GetUserName();
                    variantaDisponibila mod = (from m in db.variantaDisponibila_elev
                                 where m.variantaID == variantaID && m.elevId ==userName
                                 select m).FirstOrDefault();
                    mod.activ = false;
                    db.Entry(mod).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { ok = true });
            }
            catch
            {
                return Json(new
                {
                    ok = false
                });
            }
        }

        //completeaza s2_e_b_x_form2 cu restul !!!!
        public variantaRezolvata getSolvedXml(string xml)
        {

            //   var xml = db.variantsTable.Find(id).xml;
            StringReader strReader = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            object obj = null;
            try
            {
                strReader = new StringReader(xml);
                serializer = new XmlSerializer(typeof(variantaRezolvata));
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }
            var x = obj as variantaRezolvata;
            return x;
        }
        [HttpGet]
        public ActionResult corectareSesiune(int variantaID)
        {
            var list = (from m in db.variantsTable
                        where m.variantaID == variantaID && m.corectata==false
                        select m).ToList();
            List<variantaRezolvata> lista = new List<variantaRezolvata>();

            foreach (var item in list)
            {
                lista.Add(getSolvedXml(item.xml));
            }
            ViewBag.varID = variantaID;

            return View(lista);
        }

        [HttpGet]
        public ActionResult expectRes_s1A(int variantaID)
        {

            return PartialView("expectRes_s1A", repository.expected_s1A(variantaID));
        }
        [HttpGet]
        public ActionResult expectRes_s1B(int variantaID)
        {
            var varianta = repository.getFullTest(variantaID);
            ViewBag.sub1 = varianta.s1model.s1b1.corect;
            ViewBag.ans1 = varianta.s1model.s1B.answer_r1;
            ViewBag.ans2 = varianta.s1model.s1B.answer_r2;
            ViewBag.ans3 = varianta.s1model.s1B.answer_r3;
            ViewBag.ans4 = varianta.s1model.s1B.answer_r4;
            ViewBag.ans5 = varianta.s1model.s1B.answer_r5;
            ViewBag.ans6 = varianta.s1model.s1B.answer_r6;

            return PartialView("expectRes_s1B");
        }
        [HttpGet]
        public ActionResult expectRes_s2A()
        {

            return PartialView("expectRes_s2A");
        }
        [HttpGet]
        public ActionResult expectRes_s2B()
        {

            return PartialView("expectRes_s2B");
        }
        [HttpGet]
        public ActionResult expectRes_s2C()
        {

            return PartialView("expectRes_s2C");
        }
        [HttpGet]
        public ActionResult expectRes_s2D()
        {

            return PartialView("expectRes_s2D");
        }
        [HttpGet]
        public ActionResult expectRes_s2E()
        {

            return PartialView("expectRes_s2E");
        }
        [HttpGet]
        public ActionResult expectRes_s3A()
        {

            return PartialView("expectRes_s3A");
        }
        [HttpGet]
        public ActionResult expectRes_s3B()
        {

            return PartialView("expectRes_s3B");
        }

        [HttpPost]
        public JsonResult salveazaRezultateSesiune(List<rezultateVariantaElev> list, List<variantaCorectata> listCorect)
        {
            try
            {
                //varCorectata_profesor mod = (from m in db.varianteCorectate_Tabel
                //                             where m.variantaId == list[0].varianta_ID
                //                             select m).FirstOrDefault();
                //mod.corectata = true;
                //db.Entry(mod).State = System.Data.Entity.EntityState.Modified;
                //db.SaveChanges();
                var variantaID = list[0].varianta_ID;
                
                for (int i = 0; i < list.Count; i++)
                {


                    list[i].punctajFinal = 10 + list[i].s1_A + list[i].s1_B + list[i].s2_A + list[i].s2_B + list[i].s2_C + list[i].s2_D + list[i].s3_A + list[i].s3_B + list[i].s3_C + list[i].s3_D;
                    list[i].data = DateTime.Now;
                    list[i].xml_paginaCorectare = obtine_xml_VariantaCorectata(listCorect[i]);
                    repository.adaugaRezultatTestare(list[i]);
                    var elev = list[i].numeElev;
                    var model = (from m in db.variantsTable
                                              where m.variantaID == variantaID && m.UserId == elev
                                              select m).FirstOrDefault();
                    model.corectata = true;
                    db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                }
                return Json(new { ok = true });
            }
            catch
            {
                return Json(new { ok = false });
            }


        }

        public string obtine_xml_VariantaCorectata(variantaCorectata var)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(var.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, var);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }

            }
            return sw.ToString();
        }


        [HttpGet]
        public ActionResult lista_testariActiveElev()
        {

            return View();
        }

        [HttpGet]
        public ActionResult lista_rezultateTestari()
        {

            return View(repository.getList_rezultateVariantaElev_byName(User.Identity.GetUserName()));
        }

        [HttpGet]
        public ActionResult vizualizare_elev_variantaCompletaCorectat(int modelId)
        {
            var model = db.rezultateVar_table.Find(modelId);
            ViewBag.model = model;
            string nume = User.Identity.GetUserName();
            var raspunsuriModel = (from m in db.variantsTable
                                   where m.UserId == nume && m.variantaID==model.varianta_ID
                                   select m.xml).FirstOrDefault();
            ViewBag.raspunsuriModel = getSolvedXml(raspunsuriModel);
            ViewBag.varColor = variantaCorectata_toxml(model.xml_paginaCorectare);

            return View(repository.getFullTest(model.varianta_ID));
        }

        //[HttpGet]
        //public JsonResult obtine_Punctaj_variantaCorectata_partial(int varID)
        //{


        //}

        public variantaCorectata variantaCorectata_toxml(string xml)
        {

            //   var xml = db.variantsTable.Find(id).xml;
            StringReader strReader = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            object obj = null;
            try
            {
                strReader = new StringReader(xml);
                serializer = new XmlSerializer(typeof(variantaCorectata));
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }
            var x = obj as variantaCorectata;
            return x;
        }

    }
}