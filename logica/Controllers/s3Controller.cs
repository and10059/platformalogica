﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using logica.Models;

namespace logica.Controllers
{
    [Authorize]
    public class s3Controller : Controller
    {
        private repo repo = new repo();
        private logicaDb db = new logicaDb();
        public ActionResult createS3()
        {

            return PartialView("createS3");
        }

        [HttpGet]
        public ActionResult figuriSilogistice()
        {
            return PartialView("figuriSilogistice", db.s3SilogisTable.ToList());
        }
        [HttpGet]
        public ActionResult s3CModal()
        {
            return PartialView("s3CModal", db.s3Ctable.ToList());
        }

        [HttpPost]
        public JsonResult addNewSilog(s3Silogism model)
        {
            try
            {
                return Json(new { ok = true, model = repo.addNewSilog(model) });
            }
            catch
            {
                return Json(new { ok = false });
            }

            
        }

        [HttpPost]
        public JsonResult addNewS3C(s3C model)
        {
            try
            {
                return Json(new { ok = true, model = repo.addNewS3C(model) });
            }
            catch
            {
                return Json(new { ok = false });
            }


        }

        [HttpPost]
        public JsonResult createS3(s3Cerinta model)
        {

            try
            {
                return Json(new { ok = true, model = repo.addNewS3(model) });
            }
            catch
            {
                return Json(new { ok = false, });
            }
        }

    }
}