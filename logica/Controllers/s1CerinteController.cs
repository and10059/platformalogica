﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using logica.Models;
namespace logica.Controllers
{
    [Authorize]
    public class s1CerinteController : Controller
    {
        private logicaDb db = new logicaDb();
        private repo repository = new repo();

        public ActionResult s1Index()
        {


            return View(db.s1Table.ToList());
        }

        [HttpPost]
        public JsonResult addS1(s1Cerinta model)
        {
            try
            {

                return Json(new { ok = true, model = repository.addS1Cerinta(model) });
            }
            catch
            {
                return Json(new { ok = false });
            }


        }

        [HttpGet]
        public ActionResult s1AddModal()
        {
            return PartialView("s1AddModal");
        }

        [HttpGet]
        public ActionResult variantaNoua()
        {

            return PartialView("variantaNoua", db.s1Table.ToList());
        }


        [HttpPost]
        public JsonResult variantaNoua(List<int> cerinte, int s1B)
        {
            try
            {

                return Json(new { ok = true, s1model = repository.addNewVarS1(cerinte, s1B) });
            }
            catch
            {
                return Json(new { ok = false });
            }
        }
        [HttpGet]
        public ActionResult listaVariante()
        {

            return View(db.s1VarianteTable.ToList());
        }
        [HttpGet]
        public ActionResult variantaModal(int id)
        {

            return PartialView("variantaModal", repository.getS1GrilaByID(id));
        }

        [HttpPost]
        public JsonResult deleteVar(int id)
        {
            try
            {
                repository.removeVar(id);
            }
            catch
            {

                return Json(new { ok = false });

            }

            return Json(new { ok = true });
        }

        [HttpGet]
        public ActionResult s1bPartial()
        {

            return PartialView("s1bPartial", db.s1BcerinteTable.ToList());
        }

        [HttpGet]
        public ActionResult s1BAddModal()
        {
            return PartialView("s1BAddModal");
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult addS1B(s1B model, s1B_1 svg)
        {
            try
            {

                return Json(new { ok = true, model = repository.addS1BCerinta(model, svg) });
            }
            catch
            {
                return Json(new { ok = false });
            }


        }
        [HttpPost]
        public JsonResult deleteS1Cerinta(int id)
        {
            try
            {
                repository.removeS1Cerinta(id);
            }
            catch
            {

                return Json(new { ok = false });

            }

            return Json(new { ok = true });
        }
        [HttpPost]
        public JsonResult deleteS1BCerinta(int id)
        {
            try
            {
                repository.removeS1BCerinta(id);
            }
            catch
            {

                return Json(new { ok = false });

            }

            return Json(new { ok = true });
        }
        [HttpGet]
        public ActionResult b1()
        {

            return PartialView("b1");
        }

        [HttpGet]
        public JsonResult s1b1Modal(int id)
        {
            var model = repository.getS1B1ById(id);
            var svg = "";
            switch (model.corect)
            {
                case 1:
                    svg = model.svgObj1;
                    break;

                case 2:
                    svg = model.svgObj2;
                    break;

                case 3:
                    svg = model.svgObj3;
                    break;

                case 4:
                    svg = model.svgObj4;
                    break;
            }

            return Json(new { svg = svg }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult cerintaEditor(int id)
        {
            try
            {
                return PartialView("cerintaEditor",repository.obtine_s1Cerinta_dupaId(id));
            }
            catch
            {
                ViewBag.error = "Nu s-a putut obtine editorul pentru ceritna";
                return PartialView("cerintaEditor");
            }
        }

    }
}