namespace logica.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.variantaFinalas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        finalVarName = c.String(),
                        s1 = c.Int(nullable: false),
                        s2 = c.Int(nullable: false),
                        s3 = c.Int(nullable: false),
                        createTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.rezultateVariantaElevs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        varianta_ID = c.Int(nullable: false),
                        numeElev = c.String(),
                        punctajFinal = c.Int(),
                        s1_A = c.Int(nullable: false),
                        s1_B = c.Int(nullable: false),
                        s2_A = c.Int(nullable: false),
                        s2_B = c.Int(nullable: false),
                        s2_C = c.Int(nullable: false),
                        s2_D = c.Int(nullable: false),
                        s2_E = c.Int(nullable: false),
                        s3_A = c.Int(nullable: false),
                        s3_B = c.Int(nullable: false),
                        s3_C = c.Int(nullable: false),
                        s3_D = c.Int(nullable: false),
                        data = c.DateTime(nullable: false),
                        xml_paginaCorectare = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.s1BWithVar",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        var = c.Int(nullable: false),
                        cerinta = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s1B",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        cerinta = c.String(),
                        r1 = c.String(),
                        r2 = c.String(),
                        r3 = c.String(),
                        r4 = c.String(),
                        r5 = c.String(),
                        r6 = c.String(),
                        answer_r1 = c.Boolean(nullable: false),
                        answer_r2 = c.Boolean(nullable: false),
                        answer_r3 = c.Boolean(nullable: false),
                        answer_r4 = c.Boolean(nullable: false),
                        answer_r5 = c.Boolean(nullable: false),
                        answer_r6 = c.Boolean(nullable: false),
                        svgId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s1B_1",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        corect = c.Int(nullable: false),
                        svgObj1 = c.String(),
                        svgObj2 = c.String(),
                        svgObj3 = c.String(),
                        svgObj4 = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s1Grila",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        var = c.Int(nullable: false),
                        cerinta = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s1Cerinta",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        nume = c.String(),
                        cerinta = c.String(),
                        r1 = c.String(),
                        r2 = c.String(),
                        r3 = c.String(),
                        r4 = c.String(),
                        answer = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.variantas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        s1Bid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s2Cerinta",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        p1ID = c.Int(nullable: false),
                        p2ID = c.Int(nullable: false),
                        p3ID = c.Int(nullable: false),
                        p4ID = c.Int(nullable: false),
                        A_1 = c.Int(nullable: false),
                        A_2 = c.Int(nullable: false),
                        b_type1 = c.String(),
                        b_type2 = c.String(),
                        b_type3 = c.String(),
                        b_type4 = c.String(),
                        b_prop1 = c.Int(nullable: false),
                        b_prop2 = c.Int(nullable: false),
                        b_prop3 = c.Int(nullable: false),
                        b_prop4 = c.Int(nullable: false),
                        C_1 = c.Int(nullable: false),
                        C_2 = c.Int(nullable: false),
                        D_prop = c.Int(nullable: false),
                        D_tip1 = c.String(),
                        D_tip2 = c.String(),
                        EID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s2E",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        xRat = c.String(),
                        yRat = c.String(),
                        answer = c.String(),
                        tipCerut = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s2_prop",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        prop = c.String(),
                        type = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s3C",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        silog = c.String(),
                        tMediu = c.String(),
                        pMin = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s3Silogism",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        maj = c.String(),
                        min = c.String(),
                        con = c.String(),
                        fig = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s3Cerinta",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        A_silogismID_1 = c.Int(nullable: false),
                        A_silogismID_2 = c.Int(nullable: false),
                        B_silog = c.String(),
                        C_ID = c.Int(nullable: false),
                        D_def = c.String(),
                        D_term = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.userDatas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        userID = c.String(),
                        nume = c.String(),
                        prenume = c.String(),
                        scoala = c.String(),
                        telefon = c.String(),
                        dataInregistrare = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.variantaDisponibilas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        variantaID = c.Int(nullable: false),
                        elevId = c.String(),
                        activ = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.varCorectata_profesor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        variantaId = c.Int(nullable: false),
                        corectata = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.s1_a_rezultateElev",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        idElev = c.String(),
                        idVarianta = c.Int(nullable: false),
                        dataOra = c.DateTime(nullable: false),
                        ultimulPunctaj = c.String(),
                        xml_listaRaspunsuri = c.String(),
                        inactiv = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.s1_a_deschise",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        xml_subiect = c.String(),
                        nume = c.String(),
                        indisponibil = c.Boolean(nullable: false),
                        createDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.variantaXmlTables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        variantaID = c.Int(nullable: false),
                        xml = c.String(),
                        corectata = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.variantaXmlTables");
            DropTable("dbo.s1_a_deschise");
            DropTable("dbo.s1_a_rezultateElev");
            DropTable("dbo.varCorectata_profesor");
            DropTable("dbo.variantaDisponibilas");
            DropTable("dbo.userDatas");
            DropTable("dbo.s3Cerinta");
            DropTable("dbo.s3Silogism");
            DropTable("dbo.s3C");
            DropTable("dbo.s2_prop");
            DropTable("dbo.s2E");
            DropTable("dbo.s2Cerinta");
            DropTable("dbo.variantas");
            DropTable("dbo.s1Cerinta");
            DropTable("dbo.s1Grila");
            DropTable("dbo.s1B_1");
            DropTable("dbo.s1B");
            DropTable("dbo.s1BWithVar");
            DropTable("dbo.rezultateVariantaElevs");
            DropTable("dbo.variantaFinalas");
        }
    }
}
